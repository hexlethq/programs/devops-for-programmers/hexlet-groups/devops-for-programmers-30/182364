terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-2"
}

resource "aws_instance" "web-terraform-homework-01" {
  ami             = "ami-0fb653ca2d3203ac1"
  instance_type   = "t2.micro"
  key_name        = "my-key-pair"
  security_groups = ["me_SG_useast2"]

  tags = {
    Name = "web-terraform-homework-01"
  }
}

resource "aws_instance" "web-terraform-homework-02" {
  ami             = "ami-0fb653ca2d3203ac1"
  instance_type   = "t2.micro"
  key_name        = "my-key-pair"
  security_groups = ["me_SG_useast2"]

  tags = {
    Name = "web-terraform-homework-02"
  }
}

resource "aws_elb" "web-terraform-lb" {
  availability_zones = ["us-east-2a"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  listener {
    instance_port      = 80
    instance_protocol  = "http"
    lb_port            = 443
    lb_protocol        = "https"
    ssl_certificate_id = "arn:aws:acm:us-east-2:764963023565:certificate/774ea9fe-e229-46e4-bea9-72e839a3aa8c"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }

  instances                   = [aws_instance.web-terraform-homework-01.id, aws_instance.web-terraform-homework-02.id]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "web-terraform-lb"
  }

}

resource "aws_route53_record" "web-terraform-domain" {
  zone_id = "Z0644285FQEA828C16Y8"
  name    = "oceandev.space"
  type    = "A"

  alias {
    name                   = aws_elb.web-terraform-lb.dns_name
    zone_id                = aws_elb.web-terraform-lb.zone_id
    evaluate_target_health = true
  }
}
